package org.arlesheim.darkgate.model;

import java.util.ArrayList;
import java.util.List;

public class TrainStation {

	public String link;
	public JapaneseContent name;
	public List<TrainLine> trainLines;
	
	public TrainStation(TrainLine trainLine, JapaneseContent name, String link) {
		super();
		this.link = link;
		this.name = name;
		this.trainLines = new ArrayList<TrainLine>();
		this.trainLines.add(trainLine);
	}
	public TrainStation(TrainLine trainLine, String name, String link) {
		this(trainLine, new JapaneseContent(name), link);
	}
	public JapaneseContent getName() {
		return name;
	}
	public void setName(JapaneseContent name) {
		this.name = name;
	}
	public void setName(String name){
		this.name = new JapaneseContent(name);
	}
	public List<TrainLine> getTrainLines() {
		return trainLines;
	}
	public void addTrainLine(TrainLine line){
		this.trainLines.add(line);
	}
	
	public String toString(){
		return name.toString();
	}
	public String getLink() {
		return link;
	}
	public String tsvData() {
		String trainLinesAsTSV = "";
		for (TrainLine trainLine : trainLines) {
			trainLinesAsTSV += trainLine.name.tsvData();
		}
		return name.tsvData() + "~http://ja.wikipedia.org" + link + "~" + trainLinesAsTSV;
	}
}
