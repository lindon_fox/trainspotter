package org.arlesheim.darkgate.model;

import java.util.ArrayList;
import java.util.List;

public class TrainLine {
	//TODO also get the train color
	public String link;
	public JapaneseContent name;
	public List<TrainStation> trainStations;
	public TrainLine(JapaneseContent name, String link) {
		super();
		this.link = link;
		this.name = name;
		this.trainStations = new ArrayList<TrainStation>();
	}
	
	public TrainLine(String name, String link) {
		this(new JapaneseContent(name), link);
	}
	public JapaneseContent getName() {
		return name;
	}
	public void setName(JapaneseContent name) {
		this.name = name;
	}
	public void setName(String name) {
		this.name = new JapaneseContent(name);
	}
	public List<TrainStation> getTrainStations() {
		return trainStations;
	}
	public void addTrainStation(TrainStation station){
		this.trainStations.add(station);
	}
	
	@Override
	public String toString() {
		String content = name.toString() + " Station Count: " + trainStations.size();
		for (TrainStation station : trainStations) {
			content += "\n>" + station.toString();
		}
		return content;
	}
}
