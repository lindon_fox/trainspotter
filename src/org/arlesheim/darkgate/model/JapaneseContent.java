package org.arlesheim.darkgate.model;

public class JapaneseContent {
	private String content;
	private String reading;
	
	public JapaneseContent(String content) {
		super();
		this.content = content;
	}
	public JapaneseContent(String content, String reading) {
		super();
		this.content = content;
		this.reading = reading;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getReading() {
		return reading;
	}
	public void setReading(String reading) {
		this.reading = reading;
	}
	
	@Override
	public String toString() {
		return content + "[" + reading + "]";
	}
	public String tsvData() {
		return content  + "~" + reading;
	}
}
