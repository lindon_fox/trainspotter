package org.arlesheim.darkgate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.arlesheim.darkgate.model.TrainLine;
import org.arlesheim.darkgate.model.TrainStation;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/*
 * 
"常磐快速線", "成田線",

 * 
 * 
 */
public class TrainSpotter {

	public static void main(String[] args){
		try {
			TrainSpotter trainSpotter = new TrainSpotter();
//			String[] trainLines = new String[] {"川越線", "京浜東北線", "根岸線", "中央・総武緩行線", "中央線快速", "埼京線", "青梅線", "五日市線", "成田線", "京葉線", "武蔵野線", "南武線", "鶴見線", "横浜線", "相模線", "八高線", "川越線", "常磐快速線", "常磐緩行線", "山手線"};
			String[] trainLines = new String[] {"川越線", "根岸線", "中央・総武緩行線", "中央線快速", "埼京線", "青梅線", "五日市線", "成田線", "京葉線", "武蔵野線", "南武線", "鶴見線", "横浜線", "相模線", "八高線", "川越線", "常磐快速線", "常磐緩行線", "山手線"};
//			String[] trainLines = new String[] {"中央・総武緩行線", "中央線快速", "山手線"};
			//Metro
//			String[] trainLines = new String[] {"東京地下鉄銀座線", "東京地下鉄丸ノ内線", "東京地下鉄日比谷線", "東京地下鉄東西線", "東京地下鉄千代田線", "東京地下鉄有楽町線", "東京地下鉄南北線", "東京地下鉄副都心線", "東京地下鉄半蔵門線"}; 
//			String[] trainLines = new String[] {"東京地下鉄東西線", "有楽町線", "南北線"};
			String link = null;// = "http://ja.wikipedia.org/wiki/山手線";
			Document document = null;//Jsoup.connect(link).get();
			TrainLine trainLine = null;
			List<TrainLine> trainLineList = new ArrayList<TrainLine>();
			for (int i = 0; i < trainLines.length; i++) {
				link = "http://ja.wikipedia.org/wiki/" + trainLines[i];
				System.out.println("LINE: Taking a look at " + trainLines[i]);
				document = Jsoup.connect(link).get();
				
				trainLine = trainSpotter.parseTrainLine(document, link);
				for (TrainStation station : trainLine.getTrainStations()) {
					link = "http://ja.wikipedia.org" + station.getLink();
					System.out.println("taking a look at " + station.toString() + " LINK: " + link);
					if(station.getLink() != null){
						document = Jsoup.connect(link).get();
						station.getName().setReading(trainSpotter.findReadingInDocument(station.getName().getContent(), document));
					}
					else{
						System.err.println("did not have a link for this station (maybe should just try with a random link): " + station);
					}
				}
				trainLineList.add(trainLine);
				System.out.println(trainLine);
			}
			
			try {
				PrintWriter out = new PrintWriter("tokyo_train_stations.tsv");
				for (TrainLine trainLine2 : trainLineList) {
					for (TrainStation trainStation : trainLine2.trainStations) {
						out.println(trainStation.tsvData());
					}
				}
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
//			TrainLine yamanoteTrainLine = trainSpotter.parseTrainLine(document, link);
//			System.out.println(yamanoteTrainLine);
//			for (TrainStation station : yamanoteTrainLine.getTrainStations()) {
//				System.out.println("taking a look at " + station.toString());
//				link = "http://ja.wikipedia.org" + station.getLink();
//				document = Jsoup.connect(link).get();
//				station.getName().setReading(trainSpotter.findReadingInDocument(station.getName().getContent(), document));
//			}
//			System.out.println(yamanoteTrainLine);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	public String findReadingInDocument(String content, Document document){
		String reading = null;
		int readingStart = document.text().indexOf(content + "（") + content.length() + 1;
		int readingEnd = document.text().indexOf("）", readingStart);
		reading = document.text().substring(readingStart, readingEnd);
		while(containsOnlyHiraganaAndKatakana(reading) == false && reading != null){
//			System.out.println(document.text());
			System.out.println("Looking for reading... " + content + " from " + readingStart + " to " + readingEnd);
			readingStart = document.text().indexOf(content + "（", readingEnd) + content.length() + 1;
			if(readingStart == content.length()){
				//nothing found...
				return null;
			}
			readingEnd = document.text().indexOf("）", readingStart);
			reading = document.text().substring(readingStart, readingEnd);
		}
		return reading;
	}
	
	public Boolean containsOnlyHiraganaAndKatakana(String reading){
		for (int i = 0; i < reading.length(); i++) {
			char character = reading.charAt(i);
			if(Character.UnicodeBlock.of(character) != Character.UnicodeBlock.HIRAGANA && Character.UnicodeBlock.of(character) != Character.UnicodeBlock.KATAKANA){
				return false;
			}
		}
		
		return true;
	}
	
	public TrainLine parseTrainLine(Document document, String link){
		String title = document.title();
		String lineName = title.replaceFirst(" - Wikipedia", "");
		lineName = lineName.replaceFirst("東京地下鉄", "");
//		System.out.println(lineName);
		TrainLine trainLine = new TrainLine(lineName, link);
		
		//need to get the reading of the train line...
//		System.out.println(document.text());
		//（やまのてせん）
//		int readingStart = document.text().indexOf(lineName + "（") + lineName.length() + 1;
//		int readingEnd = document.text().indexOf("）", readingStart);
//		String reading = document.text().substring(readingStart, readingEnd);
		trainLine.getName().setReading(findReadingInDocument(lineName, document));
//		System.out.println(reading);
		Elements elements = document.getElementsByClass("wikitable");
		//looking for the list of stations
		Element stationListTable = null;
		Boolean foundLineNameHeader = false;
		Boolean foundStationNameHeader = false;
		Boolean foundStationListTable = false;
		for(Element table : elements){
			Elements headers = table.getElementsByTag("th");
			for (Element header : headers) {
//				System.out.println(header.text());
				//requirements
				//  正式路線名
				//  駅名
				if(header.text().indexOf("正式路線名") == 0 
						|| header.text().indexOf("駅番号") == 0 
						|| header.text().indexOf("所在地") == 0 
						|| header.text().indexOf("接続路線・備考") == 0 ){
					foundLineNameHeader = true;
				}
				if(header.text().indexOf("駅名") == 0){
					foundStationNameHeader = true;
				}
			}
			if(foundLineNameHeader && foundStationNameHeader){
				System.out.println("found the station list table");
				stationListTable = table;
				foundStationListTable = true;
				Elements rows = stationListTable.getElementsByTag("td");
				for (Element row : rows) {
					String rowContents = row.text();
					rowContents = rowContents.replaceAll(Pattern.quote("[* ") + "\\d" + Pattern.quote("]"), "");
					if(rowContents.length() < 16 && rowContents.indexOf("駅") == rowContents.length() - 1 && rowContents.length() != 0){
						Elements links = row.getElementsByTag("a");
						String href = null;
						for (Element wikiLink : links) {
							if(wikiLink.attr("title").indexOf(rowContents) == 0){
								href = wikiLink.attr("href");
							}
						}
						
						trainLine.addTrainStation(new TrainStation(trainLine, rowContents, href));

						if(href == null){
							System.err.println("found a station without a link.. for " + rowContents + " here are the links': " + links);
						}

					}
				}
			}
			foundLineNameHeader = false;
			foundStationNameHeader = false;
		}
		if(foundStationListTable == false){
			System.err.println("could not find the station list tabel... " + link);
		}
		return trainLine;
	}
	
	public static void test(){

		URL url;
		InputStream is = null;
		BufferedReader bufferedReader = null;
//		DataInputStream dis;
		String line;

		try {
		    url = new URL("http://ja.wikipedia.org/wiki/%E5%B1%B1%E6%89%8B%E7%B7%9A");
		    is = url.openStream();  // throws an IOException
		    bufferedReader = new BufferedReader(new InputStreamReader(is));
//		    dis = new DataInputStream(new BufferedInputStream(is));

		    List<List<String>> tables = new ArrayList<List<String>>();
		    List<String> currentTable = null;
		    
		    while ((line = bufferedReader.readLine()) != null) {
		    	if(currentTable == null){
		    		//look for the start of a new table
		    		if(line.indexOf("<table class=\"wikitable\"") == 0){
		    			//we have a new table
		    			currentTable = new ArrayList<String>();
		    			currentTable.add(line);
		    			tables.add(currentTable);
		    		}
		    	}
		    	else{
		    		//look for the end of the current table
	    			currentTable.add(line);
		    		if(line.indexOf("</table>") == 0){
		    			currentTable = null;
		    		}
		    	}
		    }
		} catch (MalformedURLException mue) {
		     mue.printStackTrace();
		} catch (IOException ioe) {
		     ioe.printStackTrace();
		} finally {
		    try {
		        is.close();
		    } catch (IOException ioe) {
		        // nothing to see here
		    }
		}
	}
}
